# line-distance-calculator
> A small collection of methods for calculating the distance from a point to a line.

An ES6 module containing a small collection of methods for calculating the distance between a point and a line (which is optionally either multi-segmented or infinite). Written for my project [Nibriboard](https://github.com/sbrl/Nibriboard). Check out the [demo](https://sbrl.gitlab.io/line-distance-calculator/demo.html)!

It does the job as to [distance-to-line-segment](https://npmjs.org/package/distance-to-line-segment), but I ended up implemented this before I saw that package - and I wanted to have a go at implementing the algorithm successfully after I failed a few weeks ago.

## Installation
You'll need support for ES6 Modules in your project / build tool / browser in order to use this package. Install this with npm: `npm install --save line-distance-calculator`.

## Usage
There are 3 primary methods exported by this module:

### point_line_distance_multi(point, line_points)
Calculates the minimum distance between a specified multi-segmented line and a specified point. Example:

```js
import { point_line_distance_multi } from "line-distance-calculator";

let distance_data = point_line_distance_multi(
	{ x: 45, y: 30 },
	[
		{ x: 250, y: 337 },
		{ x: 266, y: 326 },
		{ x: 574, y: 177 },
		{ x: 605, y: 254 },
		{ x: 469, y: 120 },
		{ x: 377, y: 315 },
		{ x: 411, y: 328 },
		{ x: 435, y: 335 },
		{ x: 531, y: 344 }
	]
);
console.log(`The minimum distance is ${distance_data[1]}`);
console.log(`The segment that's closest to the point starts at index ${distance_data[0]}`);
```

### point_line_distance(point, line_start, line_end)
Calculates the minimum distance between a point and an infinite line specified by 2 more points. Example:

```js
import { point_line_distance } from "line-distance-calculator";

let point = { x: 50, y: 82 };
let line_start = { x: 10, y: 52 };
let line_end = { x: 50, y: 18 };

let distance = point_line_distance(point, line_start, line_end);

console.log(`The distance to the line from the point is ${distance}`);
```

### point_infinite_line_distance(point, line_a, line_b)
An implementation from [this wikipedia article](https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points) before I realised that the algorithm was for an infinite line and not a finite one. I've left this in because this implementation does work - it's just not what I wanted myself.

It works identically to `point_line_distance` demonstrated above. Example:

```js
import { point_infinite_line_distance } from "line-distance-calculator";

let point = { x: 50, y: 82 };
let line_start = { x: 10, y: 52 };
let line_end = { x: 50, y: 18 };

let distance = point_infinite_line_distance(point, line_start, line_end);

console.log(`The distance to the infinite line from the point is ${distance}`);
```

## Contribute
Contributions are welcome! If you find a bug, please [open an issue](https://gitlab.com/sbrl/line-distance-calculator/issues/new) - or even better [open a pull request](https://gitlab.com/sbrl/line-distance-calculator/merge_requests/new) :smiley_cat:


## License
This package is licensed under the _Mozilla Public License 2.0_. The full license text is available [here](https://gitlab.com/sbrl/line-distance-calculator/blob/master/LICENSE) - along with a link to a summary summary on what you can and can't do with it.

If you'd like to do something that's prohibited by the license - please do get in touch! My email address is available on my [website](https://starbeamrainbowlabs.com/).

## Sources
 - [Lines and Distance of a Point to a Line](http://geomalgorithms.com/a02-_lines.html) on [Geometry Algorithms Home](http://geomalgorithms.com/) by Dan Sunday
