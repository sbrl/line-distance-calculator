"use strict";

import { point_line_distance_multi } from "./distance_detector.js";

/**
 * @template https://gist.github.com/sbrl/f8b584a383116b38da29
 */
class Renderer
{
	constructor(canvas)
	{
		this.canvas = canvas;
		this.context = canvas.getContext("2d");
		
		this.trackWindowSize();
		
		// Track the left mouse button state
		this.mouse_down = false;
		this.mouse_position = { x: 0, y: 0 };
		
		this.canvas.addEventListener("mousedown", ((event) => {
			document.getElementById("output-mouse-down").checked = this.mouse_down = true;
			this.line_points.length = 0;
		}).bind(this));
		this.canvas.addEventListener("mouseup", ((event) => {
			document.getElementById("output-mouse-down").checked = this.mouse_down = false;
		}).bind(this));
		this.canvas.addEventListener("mousemove", ((event) => {
			this.mouse_position = { x: event.clientX, y: event.clientY };
			if(!this.mouse_down) return;
			this.line_points.push({ x: event.clientX, y: event.clientY });
		}));
		
		this.line_points = [];
	}
	
	nextFrame()
	{
		this.update();
		this.render(this.canvas, this.context);
		requestAnimationFrame(this.nextFrame.bind(this));
	}
	
	update()
	{
		if(this.line_points.length >= 2) {
			let distance = point_line_distance_multi(
				this.mouse_position,
				this.line_points
			);
			document.getElementById("output-distance").value = `${distance[1].toFixed(2)} (segment ${distance[0]})`;
		}
	}
	
	render(canvas, context)
	{
		context.clearRect(0, 0, canvas.width, canvas.height);
		
		if(this.line_points.length > 0) {
			context.beginPath();
			context.moveTo(this.line_points[0].x, this.line_points[0].y);
			for (let point of this.line_points)
				context.lineTo(point.x, point.y);
			
			context.lineWidth = 2;
			context.strokeStyle = "#345ee1";
			context.stroke();
		}
	}
	
	/**
	 * Updates the canvas size to match the current viewport size.
	 */
	matchWindowSize() {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
	}
	
	/**
	 * Makes the canvas size track the window size.
	 */
	trackWindowSize() {
		this.matchWindowSize();
		window.addEventListener("resize", this.matchWindowSize.bind(this));
	}
}

window.addEventListener("load", function (event) {
	var canvas = document.getElementById("canvas-main"),
		renderer = new Renderer(canvas);
	renderer.nextFrame();
	window.renderer = renderer;
	document.getElementById("message-es6-modules").style.display = "none";
});
