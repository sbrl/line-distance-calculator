"use strict";

import Vector from "./lib/Vector.js";

/**
 * Calculates the minimum distance between a specified multi-segmented line and 
 * a specified point.
 * @param	{{x,y}}		point		The point to fetch the minimum distance for.
 * @param	{[{x,y}]}	line_points	An array of points that makes up the multi-segmented line.
 * @return	{[number,number]}		The index of the starting point of the segment the point is closest to, followed by the distance to said segment.
 */
function point_line_distance_multi(point, line_points) {
	if(!(line_points instanceof Array) || line_points.length < 2)
		throw new Error("Error: argument 2 should be an array with at least 2 points.");
	
	let min_distance_index = 0;
	let min_distance = point_line_distance(point, line_points[0], line_points[1]);
	for(let i = 1; i < line_points.length - 1; i++) {
		let this_distance = point_line_distance(
			point,
			line_points[i],
			line_points[i + 1]
		);
		
		if(this_distance < min_distance) {
			min_distance_index = i;
			min_distance = this_distance;
		}
	}
	
	return [ min_distance_index, min_distance ];
}

/**
 * Calculates the minimum distance between a point and an infinite line specified by 2 more points.
 * @param	{{x,y}}		point		The point to find the distance from.
 * @param	{{x,y}}		line_start	The starting point of the line.
 * @param	{{x,y}}		line_end	The ending point of the line.
 * @return	{number}	The minimum distance to the line.
 */
function point_line_distance(point, line_start, line_end) {
	if(!(point instanceof Vector))
		point = new Vector(point.x, point.y);
	if(!(line_start instanceof Vector))
		line_start = new Vector(line_start.x, line_start.y);
	if(!(line_end instanceof Vector))
		line_end = new Vector(line_end.x, line_end.y);
	
	let v = line_start.distanceTo(line_end);
	let w = line_start.distanceTo(point);
	
	let c1 = w.dotProduct(v);
	
	if(c1 <= 0)
		return Math.sqrt(w.dotProduct(w));
	
	let c2 = v.dotProduct(v);
	if(c2 <= c1) {
		let u = line_end.distanceTo(point);
		return Math.sqrt(u.dotProduct(u));
	}
	
	let b = c1 / c2;
	let point_b = line_start.clone().add(v.multiply(b));
	
	let u = point_b.distanceTo(point);
	return Math.sqrt(u.dotProduct(u));
}

/**
 * Calculates the minimum distance between a point and an infinite line specified by 2 more points.
 * Ported from C++ code found at http://geomalgorithms.com/a02-_lines.html
 * @param	{{x,y}}		point		The point to find the distance from.
 * @param	{{x,y}}		line_start	The starting point of the line.
 * @param	{{x,y}}		line_end	The ending point of the line.
 * @return	{number}	The minimum distance to the line.
 */
function point_infinite_line_distance(point, line_start, line_end) {
	return (
		Math.abs(
			point.x * (line_end.y - line_start.y) - 
			point.y * (line_end.x - line_start.x) +
			line_end.x * line_start.y -
			line_end.y * line_start.x
		) / Math.sqrt(
			(line_end.y - line_start.y) * (line_end.y - line_start.y) + 
			(line_end.x - line_start.x) * (line_end.x - line_start.x)
		)
	);
}

export { point_line_distance_multi, point_line_distance };
